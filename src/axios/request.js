import axios from 'axios'
import { VueAxios } from './axios'
// import { ElNotification } from 'element-plus'

// 创建 axios 实例
const service = axios.create({
  baseURL: '', // api base_url
  timeout: 30000, // 请求超时时间,
  headers: {
    'Content-Type': 'application/json',
    'tk': 'e55efd4c202d4e6aac01cda0d54935df'
  }
})

// 后台报错统一处理 --- 绑定了element UI框架的提示框
// const err = (error) => {
//   if (error.response) {
//     const { data } = error.response
//     if (error.response.status === 403) {
//       ElNotification.error({
//         title: 'Forbidden',
//         message: data.message
//       })
//     } else if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
//       ElNotification.error({
//         title: 'Unauthorized',
//         message: 'Authorization verification failed'
//       })
//     } else {
//       ElNotification.error({
//         title: '系统错误',
//         message: data.message || '请联系管理员！'
//       })
//     }
//   }
//   return Promise.reject(error)
// }

// request去的请求头处理
// service.interceptors.request.use(
//   config => {
//     const token = Vue.prototype.$keycloak.token
//     if (token) {
//       config.headers.Authorization = 'Bearer ' + token // 让每个请求携带自定义 token 请根据实际情况自行修改i
//     }
//     return config
//   }
// )

// response返回数据的格式
service.interceptors.response.use(response => response.data)

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, service)
  }
}

function get(url, params, config) {
  const serviceConfig = Object.assign({
    method: 'GET',
    url,
    params: params
  }, config)

  return new Promise((resolve, reject) => {
    service(serviceConfig).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

function post(url, data, config) {
  const serviceConfig = Object.assign({
    method: 'POST',
    url,
    data: data,
    headers: {
      'Content-Type': 'application/json;'
    }
  }, config)

  return new Promise((resolve, reject) => {
    service(serviceConfig).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

function put(url, data, config) {
  const serviceConfig = Object.assign({
    method: 'PUT',
    url,
    data: data,
    headers: {
      'Content-Type': 'application/json;'
    }
  }, config)

  return new Promise((resolve, reject) => {
    service(serviceConfig).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

function del(url, data, config) {
  const serviceConfig = Object.assign({
    method: 'DELETE',
    url,
    data: data,
    headers: {
      'Content-Type': 'application/json;'
    }
  }, config)

  return new Promise((resolve, reject) => {
    service(serviceConfig).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

/**
 * 下载文件 用于excel导出
 * @param url
 * @param parameter
 * @returns {*}
 */
function downFile(url, parameter, config) {
  const serviceConfig = Object.assign({
    url: url,
    params: parameter,
    method: 'get',
    responseType: 'blob'
  }, config)

  return service(serviceConfig)
}

// 导出excel乱码处理，加了服务器数据类型
function exportExcel(url, params) {
  return new Promise((resolve, reject) => {
    service({
      method: 'GET',
      url,
      params: params,
      responseType: 'arraybuffer'
    })
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

export {
  installer as VueAxios,
  service as axios,
  get,
  post,
  del,
  put,
  downFile,
  exportExcel
}
