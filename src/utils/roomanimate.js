import { TWEEN } from '../../public/lib/tween.module.min.js'

// 处理建筑数据
function disposeRoomData(mainGroup) {
  const animateCfg = {
    room: {
      building: null,
      accor: null,
      cameraPosition: [-105, 137, 140]
      // labelGroup: that.labelGroup.children[1]
    }
  }
  animateCfg.room.building = mainGroup
  animateCfg.room.accor = createRoomAccordionGroup(animateCfg.room.building, {
    // type: 2,
    distence: 300,
    duration: 1500
  })

  console.log('animateCfg====>', animateCfg)
  return animateCfg
}

function createTween(option) {
  // 是否先删除所有的TWEEN, 默认否,
  option.removeAll ? TWEEN.removeAll() : ''
  // 开启更新
  const tween = new TWEEN.Tween(option.startObject).to(option.endObject, option.duration).easing(option.easing || TWEEN.Easing.Cubic.InOut) // TWEEN.Easing.Cubic.InOut //匀速
  // debugger
  tween.onStart(() => {
    if (option.controlsDisabled && this.controls) this.controls.enabled = false
    option.start ? option.start(tween._object) : ''
  })
  tween.onUpdate(() => {
    option.update ? option.update(tween._object) : ''
  })
  tween.onComplete(() => {
    if (option.controlsDisabled && this.controls) this.controls.enabled = true
    TWEEN.remove(tween)
    option.done ? option.done() : ''
  })
  tween.onStop(() => {
    option.stop ? option.stop() : ''
  })
  TWEEN.add(tween)
  tween.start()
  return tween
}

// 降落动画
class DescentGroup {
  constructor(group, option) {
    this.group = group
    this.option = option
    this.typeD = option.type === 2 ? 'x' : 'y'
    this.group.children.forEach(element => { // 记录初始值
      element.userData.positionY0 = element.position[this.typeD]
      element.userData.positionY1 = element.position[this.typeD] - this.option.distence
    })
  }

  go(index) {
    const that = this

    return new Promise((resolve, reject) => {
      const element = this.group.children[index]
      createTween({
        controls: this.option.controls,
        startObject: {
          y: element.position[that.typeD] // 当前值
        },
        endObject: {
          y: element.userData.positionY1 // 走或者回到初始值
        },
        duration: that.option.duration || 1000,
        // ...that.data,
        update(object) {
          element.position[that.typeD] = object.y

        },
        done() {
          resolve()
        }
      })
    })
  }
}

// 手风琴打散
class AccordionGroup {
  constructor(group, option) {
    this.group = group
    this.option = option
    this.typeD = option.type === 2 ? 'x' : 'y'
    this.group.children.forEach(element => { // 记录初始值
      element.children.forEach(el => {
        el.userData.positionY0 = el.coordinates[2]
        el.userData.positionY1 = el.coordinates[2] + this.option.distence
      })
    })
  }

  go(index) {
    const that = this
    return new Promise((resolve, reject) => {
      this.group.children.forEach((element, i) => {
        element.children.forEach((el) => {
          createTween({
            startObject: {
              y: el.coordinates[2] // 当前值
            },
            endObject: {
              y: i > index ?  el.userData.positionY1 : el.userData.positionY0 // 走或者回到初始值
            },
            duration: that.option.duration || 1000,
            ...that.data,
            update(object) {
              const center = el.coordinates
              el.setCoords([center[0], center[1], object.y])
            },
            done() {
              resolve()
            }
          })
        })
      })
    })
  }
}

/**
 * 手风琴打散
 * @param {*} group
 * @param {*} data
 */
function createRoomAccordionGroup(group, data) {
  return new AccordionGroup(group, data)
}

/**
 * 手风琴打散
 * @param {*} group
 * @param {*} data
 */
function createRoomDescentGroup(group, data) {
  return new DescentGroup(group, data)
}

export { disposeRoomData }
