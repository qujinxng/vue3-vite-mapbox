// 封装地图相关的公用方法

export function flashAnimatePoint(width, height, color, map) {
  const pulsingDot = {
    width: width,
    height: height,
    data: new Uint8Array(width * height * 4),

    // When the layer is added to the map,
    // get the rendering context for the map canvas.
    onAdd: function() {
      const canvas = document.createElement('canvas')
      canvas.width = this.width
      canvas.height = this.height
      this.context = canvas.getContext('2d')
    },

    // Call once before every frame where the icon will be used.
    render: function() {
      const duration = 1000
      const t = performance.now() % duration / duration

      const radius = width / 4 * 0.3
      const outerRadius = width / 4 * 0.7 * t + radius
      const context = this.context

      // Draw the outer circle.
      context.clearRect(0, 0, this.width, this.height)
      context.beginPath()
      context.arc(
        this.width / 2,
        this.height / 2,
        outerRadius,
        0,
        Math.PI * 2
      )
      if (color === '绿') {
        context.fillStyle = `rgba(200, 255, 200, ${1 - t})`
      } else if (color === '黄') {
        context.fillStyle = `rgba(255, 255, 200, ${1 - t})`
      }
      context.fill()

      const t2 = performance.now() % 2000 / 2000
      context.beginPath()
      context.arc(
        this.width / 2,
        this.height / 2,
        outerRadius * 2,
        0,
        Math.PI * 2
      )
      if (color === '绿') {
        context.fillStyle = `rgba(200, 255, 200, ${1 - t2})`
      } else if (color === '黄') {
        context.fillStyle = `rgba(255, 255, 200, ${1 - t2})`
      }
      context.fill()

      // Draw the inner circle.
      context.beginPath()
      context.arc(
        this.width / 2,
        this.height / 2,
        radius,
        0,
        Math.PI * 2
      )
      if (color === '绿') {
        context.fillStyle = 'rgba(100, 255, 100, 1)'
        context.strokeStyle = 'rgba(100, 255, 100, 1)'
      } else if (color === '黄') {
        context.fillStyle = 'rgba(254,192,61, 1)'
        context.strokeStyle = 'rgba(254,192,61, 1)'
      }
      // context.lineWidth = 2 + 4 * (1 - t)
      context.fill()
      context.stroke()

      // 透明渐变竖线
      const gnt1 = context.createLinearGradient(this.width / 2, this.height / 2, this.width / 2, this.height)
      if (color === '绿') {
        gnt1.addColorStop(0.5, 'rgba(100, 255, 100, 0.5)')
        gnt1.addColorStop(0, 'rgba(100, 255, 100, 1)')
        gnt1.addColorStop(1, 'rgba(100, 255, 100, 0)')
      } else if (color === '黄') {
        gnt1.addColorStop(0.5, 'rgba(254,192,61, 0.5)')
        gnt1.addColorStop(0, 'rgba(254,192,61, 1)')
        gnt1.addColorStop(1, 'rgba(254,192,61, 0)')
      }

      context.strokeStyle = gnt1
      context.lineWidth = 4
      context.moveTo(this.width / 2, this.height / 2)
      context.lineTo(this.width / 2, this.height)
      context.stroke()
      // Update this image's data with data from the canvas.
      this.data = context.getImageData(
        0,
        0,
        this.width,
        this.height
      ).data

      // Continuously repaint the map, resulting
      // in the smooth animation of the dot.
      map.triggerRepaint()

      // Return `true` to let the map know that the image was updated.
      return true
    }
  }
  return pulsingDot
}
