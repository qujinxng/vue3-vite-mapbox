function 合并多个工作薄间多个工作表() {
  const twb = ThisWorkbook
  const pah = twb.Path
  const str = pah + '\\*.et'
  let fn = Dir(str)
  while (fn != twb.Name) {
    Workbooks.Open(pah + '\\' + fn)
    const awb = ActiveWorkbook
    awb.Sheets.Copy(null, twb.Sheets(twb.Sheets.Count))
    awb.Close(false)
    try {
      fn = Dir()
    } catch (err) {
      break
    }
  }
}

function t2(rng) {

  const arr1 = WorksheetFunction.Transpose(rng)

  const arr = WorksheetFunction.Transpose(arr1)

  const brr = arr.flat(), l = arr1.length//flat数组“拉平”，变成一维数组

  return arr[0].map((v, i) => brr.slice(i * l, i * l + l))//map返回列表,slice在数组中读取指定元素

}

function t3() {

  let n = 1
  Sheets('汇总').UsedRange.ClearContents()
  Range('a1:k1').Value2 = ['订单号', '订单日期', '区域', '省份', '城市', '产品类别', '产品子类别', '产品名称', '产品包箱', '运送日期', '表明']

  for (const sht of Sheets) {

    if (sht.Name != '汇总') {

      const rng = sht.Range('a2', sht.Cells(Rows.Count, 10).End(3))
      const arr = t2(rng)

      for (const ar of arr) {
        ar.push(sht.Name)//数组中添加新元素
        Range('a1:k1').Offset(n++).Value2 = ar//
      }

    }

  }

  Range('b:b').NumberFormatLocal = 'yyyy/m/d;@'

  Range('J:J').NumberFormatLocal = 'yyyy"年"m"月"d"日";@'

}

function 合并工作表数据() {
  var NewArr = [], n = 1;
  for (var ws of Sheets) {
    var Arr = ws.Range("a1").CurrentRegion.Value();
    if (n++ == 1) { 
      var title = Arr[0].concat("工作表名") 
    }; 
    delete Arr[0]Arr.forEach(ar => NewArr.push(ar.concat(ws.Name)));
  } 
  NewArr.unshift(title); 
  var wb = Workbooks.Add(); 
  wb.Sheets(1).Range("a1").Resize(NewArr.length, NewArr[0].length).Value2 = NewArr;
}

function 合并工作表数据(){
  var NewArr=[],n=1;
  for (var ws of Sheets){
    var Arr=ws.Range("a1").CurrentRegion.Value();
    if (n++ ==1){
      var title=Arr[0].concat("工作表名")
    };
    delete Arr[0]
    Arr.forEach(ar=>NewArr.push(ar.concat(ws.Name)));
  }
  NewArr.unshift(title);
  var wb=Workbooks.Add();
  wb.Sheets(1).Range("a1").Resize(NewArr.length,NewArr[0].length).Value2=NewArr;
}

function 合并工作表数据()
{
var NewArr = [], n = 1;
  // 遍历tab表
  for (var ws of Sheets) {
    // 从表中a1位置开始获取数据，截止到有空白行位置
    var Arr = ws.Range("a1").CurrentRegion.Value();
    if (n++ == 1) { 
      // 获取首个表的表头，并新增‘工作表名’字段
      var title = Arr[0].concat("工作表名") 
    }; 
    // 删除表头
    delete Arr[0]
    // 循环当前表中数据添加到数组中，并在最后追加表名
    Arr.forEach(ar => NewArr.push(ar.concat(ws.Name)));
  } 
  // 在数据首位新增表头
  NewArr.unshift(title); 
//  var wb = Worksheets.Add(); 
//  wb.Range("a1").Resize(NewArr.length, NewArr[0].length).Value2 = NewArr;
  // 将数据赋值给‘Sheet1’表中
  Sheets('Sheet1').Range("a1").Resize(NewArr.length, NewArr[0].length).Value2 = NewArr;
}


function 合并工作表数据()
{
var NewArr = [], n = 1;
  // 遍历tab表
  for (var ws of Sheets) {
    // 从表中a1位置开始获取数据，截止到有空白行位置
    var Arr = ws.Range("a1").CurrentRegion.Value();
    if (n++ == 1) { 
      // 获取首个表的表头，并新增‘工作表名’字段
      var title = Arr[0]
    }; 
    // 删除表头
    delete Arr[0]
    // 循环当前表中数据添加到数组中，并在最后追加表名
    Arr.forEach(ar => NewArr.push(ar));
  } 
  // 在数据首位新增表头
  NewArr.unshift(title); 
  // 将数据赋值给‘Sheet1’表中
  Sheets('Sheet1').Range("a1").Resize(NewArr.length, NewArr[0].length).Value2 = NewArr;
}