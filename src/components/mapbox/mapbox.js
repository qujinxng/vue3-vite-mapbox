import mapboxgl from 'mapbox-gl'

mapboxgl.accessToken =
  'pk.eyJ1IjoibGlud2VpOTIxMjE5IiwiYSI6ImNrb2pwbnM5dTFpZ3UybnB3dHlnc2M3dGsifQ.XbmGjeu_6Fr9qByZQxsvkQ'

export default mapboxgl
