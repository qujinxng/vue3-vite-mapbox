import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import HesekejiMap from '../views/mapcolor/HesekejiMap.vue'
import BaiseshenseMap from '../views/mapcolor/BaiseshenseMap.vue'
import TietujianzhuMap from '../views/mapcolor/TietujianzhuMap.vue'
import LinewidthMap from '../views/test/LinewidthMap.vue'
import CallOthremap from '../views/mapcolor/CallOthremap.vue'
import CadzshpMap from '../views/mapcolor/CadzshpMap.vue'
import ZzdxfcHome from '../views/zzdxfc/Home.vue'
import DataSummary from '../views/zzdxfc/DataSummary.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/hskj',
    name: 'Hskj',
    component: HesekejiMap
  },
  {
    path: '/bsss',
    name: 'Bsss',
    component: BaiseshenseMap
  },
  {
    path: '/jztt',
    name: 'Jztt',
    component: TietujianzhuMap
  },
  {
    path: '/line',
    name: 'Line',
    component: LinewidthMap
  },
  {
    path: '/othermap',
    name: 'Othermap',
    component: CallOthremap
  },
  {
    path: '/cadzshp',
    name: 'Cadzshp',
    component: CadzshpMap
  },
  {
    path: '/zzdxfc',
    name: 'Zzdxfc',
    component: ZzdxfcHome
  },
  {
    path: '/zzdxfcmap',
    name: 'Zzdxfcmap',
    component: DataSummary
  }
  // {
  //   path: '/edit',
  //   name: 'Edit',
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ '../views/editor/EditorModule.vue'),
  // },
  // {
  //   path: '/wtmb',
  //   name: 'Wtmb',
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ '../views/test/ObjectStroke.vue'),
  // }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
