import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'mapbox-gl/dist/mapbox-gl.css'
import 'threebox-plugin/dist/threebox.css'
import { createPinia } from 'pinia'

// ui框架全局引入
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css'

// 创建pinia 实例
const pinia = createPinia()
const app = createApp(App)

app.use(router).use(Antd).use(pinia)

app.mount('#app')
