import { get, post } from '../axios/request'

const requestPath = `/geoserver/sqsfgx/ows`
// 修改图层数据
const updataLayerData = (parameter) => {
  return post(requestPath, parameter)
}

// 获取图层数据
const getsqsfLayerData = (parameter) => {
  return get(requestPath, parameter)
}

// 获取sqsf图层组下图层数据
const getLayerData = (groupName, parameter) => {
  return get(`http://61.52.175.217:8081/geoserver/${groupName}/ows`, parameter)
}

export default {
  updataLayerData,
  getsqsfLayerData,
  getLayerData
}
