import { get, post } from '../axios/request'

// 获取表中所有数据
const getLayerData = (parameter) => {
  return get('https://gov.hnshgis.com/gisplatform/search/getList', parameter)
}
// 向表中插入数据
const insertData = (parameter) => {
  return post('https://gov.hnshgis.com/gisplatform/data/insertOne', parameter)
}
// 向表中更新数据
const upData = (parameter) => {
  return post('https://gov.hnshgis.com/gisplatform/data/update', parameter)
}
// 向表中插入数据
const delectData = (parameter) => {
  return post('https://gov.hnshgis.com/gisplatform/data/delete', parameter)
}
// 导出数据
const exportData = (parameter) => {
  return post(`https://gov.hnshgis.com/gisplatform/shape/exportShp?tab=${parameter.tab}`)
}

export default {
  getLayerData,
  insertData,
  upData,
  delectData,
  exportData
}
