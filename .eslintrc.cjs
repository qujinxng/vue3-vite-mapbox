module.exports = {
  'env': {
    'browser': true,
    'es2021': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:vue/vue3-essential'
  ],
  'overrides': [
  ],
  'parserOptions': {
    'ecmaVersion': 13,
    'sourceType': 'module',
    'ecmaFeatures': {
      'modules': true,
      'jsx': true
    },
    'requireConfigFile': false,
    'parser': '@babel/eslint-parser'
  },
  'plugins': [
    'vue'
  ],
  'globals': {
    'defineProps': 'readonly',
    'defineEmits': 'readonly',
    'defineExpose': 'readonly',
    'withDefaults': 'readonly'
  },
  /*
这里时配置规则的,自己看情况配置
* "规则名": [规则值, 规则配置]
* "off"或者0    //关闭规则关闭
* "warn"或者1    //在打开的规则作为警告（不影响退出代码）
* "error"或者2    //把规则作为一个错误（退出代码触发时为1）
* */
  'rules': {
    'semi': ['warn', 'never'], // 禁止尾部使用分号 || 强制分号结尾使用[2, 'always']
    'no-undef': 'off', // 允许出现module
    'no-console': 'warn', // 禁止出现console  process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': 'warn', // 禁止出现debugger
    'no-duplicate-case': 'warn', // switch中的case标签不能重复
    'no-empty': 'warn', // 禁止出现空语句块
    'no-extra-parens': 'warn', // 禁止不必要的括号
    'no-func-assign': 'warn', // 禁止对Function声明重新赋值
    'no-unreachable': 'warn', // 禁止出现[return|throw]之后的代码块
    'no-else-return': 'warn', // 禁止if语句中return语句之后有else块
    // 'no-empty-function': 'warn', // 禁止出现空的函数块
    'no-lone-blocks': 'warn', // 禁用不必要的嵌套块
    'no-multi-spaces': 'off', // 禁止使用多个空格
    'no-redeclare': 'warn', // 禁止多次声明同一变量
    'no-return-assign': 'warn', // 禁止在return语句中使用赋值语句
    'no-return-await': 'warn', // 禁用不必要的[return/await]
    'no-self-compare': 'warn', // 禁止自身比较表达式
    'no-useless-catch': 'warn', // 禁止不必要的catch子句
    'no-useless-return': 'warn', // 禁止不必要的return语句
    'no-mixed-spaces-and-tabs': 'warn', // 禁止空格和tab的混合缩进
    'no-trailing-spaces': 'warn', // 禁止一行结束后面不要有空格
    'no-useless-call': 'warn', // 禁止不必要的.call()和.apply()
    'no-var': 'warn', // 禁止出现var用let和const代替
    'no-delete-var': 'off', // 允许出现delete变量的使用
    'no-shadow': 'off', // 允许变量声明与外层作用域的变量同名
    'default-case': 'warn', // 要求switch语句中有default分支
    'eqeqeq': 'warn', // 要求使用 === 和 !==
    'curly': 0, // 要求所有控制语句使用一致的括号风格
    'space-before-blocks': 'warn', // 要求在块之前使用一致的空格
    'space-in-parens': 'warn', // 要求在圆括号内使用一致的空格
    'space-infix-ops': 'warn', // 要求操作符周围有空格
    'space-unary-ops': 'warn', // 要求在一元操作符前后使用一致的空格
    'switch-colon-spacing': 'warn', // 要求在switch的冒号左右有空格
    'arrow-spacing': 'warn', // 要求箭头函数的箭头前后使用一致的空格
    'camelcase': 'warn', // 要求使用骆驼拼写法命名约定
    'indent': ['warn', 2], // 要求使用JS一致缩进2个空格
    // 'max-depth': ['warn', 4], // 要求可嵌套的块的最大深度4
    'max-statements': ['warn', 100], // 要求函数块最多允许的的语句数量20
    'max-nested-callbacks': ['warn', 4], // 要求回调函数最大嵌套深度3
    'max-statements-per-line': ['warn', { 'max': 1 }], // 要求每一行中所允许的最大语句数量
    'prefer-const': [ //首选const
      2,
      {
        'ignoreReadBeforeAssign': false
      }
    ],
    'generator-star-spacing': 0, //生成器函数*的前后空格
    'quotes': [2, //要求统一使用单引号符号 `` "" ''
      'single',
      {
        'avoidEscape': true,
        'allowTemplateLiterals': true
      }
    ],

    'vue/require-default-prop': 0, // 关闭属性参数必须默认值
    'vue/singleline-html-element-content-newline': 0, // 关闭单行元素必须换行符
    'vue/multiline-html-element-content-newline': 0, // 关闭多行元素必须换行符
    // 要求每一行标签的最大属性不超4个
    // 'vue/max-attributes-per-line': ['warn', { 'singleline': 4 }],
    // 要求html标签的缩进为需要2个空格
    'vue/html-indent': ['warn', 2, {
      'attribute': 1,
      'baseIndent': 1,
      'closeBracket': 0,
      'alignAttributesVertically': true,
      'ignores': []
    }],
    // 取消关闭标签不能自闭合的限制设置
    'vue/html-self-closing': ['error', {
      'html': {
        'void': 'always',
        'normal': 'never',
        'component': 'always'
      },
      'svg': 'always',
      'math': 'always'
    }],
    'vue/attribute-hyphenation': 0, // 该规则在Vue模板的自定义组件上使用连字符属性名称。
    'vue/component-name-in-template-casing': 0, // 该规则旨在警告除Vue.js模板中配置的大小写以外的标签名称。
    'vue/html-closing-bracket-spacing': 0, // 该规则的目的是在>标记的右括号之前强制执行一致的间距样式。
    'vue/html-closing-bracket-newline': 0, // 此规则在标签的右括号之前强制使用换行符（或不使用换行符）
    'vue/no-multiple-template-root': 0, // 该规则检查模板是否包含对Vue 2有效的单个根元素。vue3中删除了可以多个根节点。
    'vue/no-v-html': 0, //此规则报告v-html指令的所有使用，以降低将潜在不安全/未转义的 html 注入浏览器导致跨站点脚本 (XSS) 攻击的风险。
    'vue/multi-word-component-names': 0, // vue文件名要使用两个单词

    'no-alert': 0,//禁止使用alert confirm prompt
    'no-array-constructor': 2,//禁止使用数组构造器
    'no-bitwise': 0,//禁止使用按位运算符
    'no-caller': 1,//禁止使用arguments.caller或arguments.callee
    'no-catch-shadow': 2,//禁止catch子句参数与外部作用域变量同名
    'no-class-assign': 2,//禁止给类赋值
    'no-cond-assign': 2,//禁止在条件表达式中使用赋值语句
    'no-const-assign': 2,//禁止修改const声明的变量
    'no-constant-condition': 2,//禁止在条件中使用常量表达式 if(true) if(1)
    'no-continue': 0,//禁止使用continue
    'no-control-regex': 2,//禁止在正则表达式中使用控制字符
    'no-div-regex': 1,//不能使用看起来像除法的正则表达式/=foo/
    'no-dupe-keys': 2,//在创建对象字面量时不允许键重复 {a:1,a:1}
    'no-dupe-args': 2,//函数参数不能重复
    'no-empty-character-class': 2,//正则表达式中的[]内容不能为空
    // 'no-empty-label': 2,//禁止使用空label
    'no-eq-null': 2,//禁止对null使用==或!=运算符
    'no-eval': 1,//禁止使用eval
    'no-ex-assign': 2,//禁止给catch语句中的异常参数赋值
    'no-extend-native': 2,//禁止扩展native对象
    'no-extra-bind': 2,//禁止不必要的函数绑定
    'no-extra-boolean-cast': 2,//禁止不必要的bool转换
    'no-extra-semi': 2,//禁止多余的冒号
    'no-fallthrough': 1,//禁止switch穿透
    'no-floating-decimal': 2,//禁止省略浮点数中的0 .5 3.
    'no-implicit-coercion': 1,//禁止隐式转换
    'no-implied-eval': 2,//禁止使用隐式eval
    'no-inline-comments': 0,//禁止行内备注
    'no-inner-declarations': [0, 'functions'],//禁止在块语句中使用声明（变量或函数）
    'no-invalid-regexp': 2,//禁止无效的正则表达式
    'no-invalid-this': 0,//禁止无效的this，只能用在构造器，类，对象字面量
    'no-irregular-whitespace': 2,//不能有不规则的空格
    'no-iterator': 2,//禁止使用__iterator__ 属性
    'no-label-var': 2,//label名不能与var声明的变量名相同
    'no-labels': 2,//禁止标签声明
    'no-lonely-if': 1,//禁止else语句内只有if语句
    // 'no-loop-func': 1,//禁止在循环中使用函数（如果没有引用外部变量不形成闭包就可以）
    'no-mixed-requires': [0, false],//声明时不能混用声明类型
    'linebreak-style': [0, 'windows'],//换行风格
    'no-multi-str': 2,//字符串不能用\换行
    'no-multiple-empty-lines': [1, { 'max': 2 }],//空行最多不能超过2行
    'no-native-reassign': 2,//不能重写native对象
    'no-negated-in-lhs': 2,//in 操作符的左边不能有!
    'no-nested-ternary': 0,//禁止使用嵌套的三目运算
    'no-new': 0,//禁止在使用new构造一个实例后不赋值
    'no-new-func': 1,//禁止使用new Function
    'no-new-object': 2,//禁止使用new Object()
    'no-new-require': 2,//禁止使用new require
    'no-new-wrappers': 2,//禁止使用new创建包装实例，new String new Boolean new Number
    'no-obj-calls': 2,//不能调用内置的全局对象，比如Math() JSON()
    'no-octal': 2,//禁止使用八进制数字
    'no-octal-escape': 2,//禁止使用八进制转义序列
    // 'no-param-reassign': 2,//禁止给参数重新赋值
    'no-path-concat': 0,//node中不能使用__dirname或__filename做路径拼接
    'no-plusplus': 0,//禁止使用++，--
    'no-process-env': 0,//禁止使用process.env
    'no-process-exit': 0,//禁止使用process.exit()
    'no-proto': 2,//禁止使用__proto__属性
    'no-regex-spaces': 2,//禁止在正则表达式字面量中使用多个空格 /foo bar/
    'no-restricted-modules': 0,//如果禁用了指定模块，使用就会报错
    'no-script-url': 0,//禁止使用javascript:void(0)
    'no-sequences': 0,//禁止使用逗号运算符
    'no-shadow-restricted-names': 2,//严格模式中规定的限制标识符不能作为声明时的变量名使用
    'no-spaced-func': 2,//函数调用时 函数名与()之间不能有空格
    'no-sparse-arrays': 2,//禁止稀疏数组， [1,,2]
    'no-sync': 0,//nodejs 禁止同步方法
    'no-ternary': 0,//禁止使用三目运算符
    'no-this-before-super': 0,//在调用super()之前不能使用this或super
    'no-throw-literal': 0,//禁止抛出字面量错误 throw "error";
    'no-undef-init': 2,//变量初始化时不能直接给它赋值为undefined
    // 'no-undefined': 2,//不能使用undefined
    'no-unexpected-multiline': 2,//避免多行表达式
    'no-underscore-dangle': 0,//标识符不能以_开头或结尾
    'no-unneeded-ternary': 2,//禁止不必要的嵌套 var isYes = answer === 1 ? true : false;
    'no-unused-expressions': 0,//禁止无用的表达式
    'no-unused-vars': [1, { 'vars': 'all', 'args': 'after-used' }],//不能有声明后未被使用的变量或参数
    // 'no-use-before-define': 2,//未定义前不能使用
    'no-void': 2,//禁用void操作符
    'no-warning-comments': [1, { 'terms': ['todo', 'fixme', 'xxx'], 'location': 'start' }],//不能有警告备注
    'no-with': 2,//禁用with
    'array-bracket-spacing': [2, 'never'],//是否允许非空数组里面有多余的空格
    'arrow-parens': 0,//箭头函数用小括号括起来
    'accessor-pairs': 0,//在对象中使用getter/setter
    'block-scoped-var': 0,//块语句中使用var
    'brace-style': [1, '1tbs'],//大括号风格
    'callback-return': 1,//避免多次调用回调什么的
    'comma-dangle': [2, 'never'],//对象字面量项尾不能有逗号
    'comma-spacing': 0,//逗号前后的空格
    'comma-style': [2, 'last'],//逗号风格，换行时在行首还是行尾
    'complexity': [0, 11],//循环复杂度
    'computed-property-spacing': [0, 'never'],//是否允许计算后的键名什么的
    'consistent-return': 0,//return 后面是否允许省略
    'consistent-this': [0, 'that'],//this别名
    'constructor-super': 0,//非派生类不能调用super，派生类必须调用super "curly": [2, "all"],//必须使用 if(){} 中的{}
    'dot-location': 0,//对象访问符的位置，换行的时候在行首还是行尾
    'dot-notation': [0, { 'allowKeywords': true }],//避免不必要的方括号
    'eol-last': 0,//文件以单一的换行符结束
    'func-names': 0,//函数表达式必须有名字
    'func-style': [0, 'declaration'],//函数风格，规定只能使用函数声明/函数表达式
    'guard-for-in': 0,//for in循环要用if语句过滤
    'handle-callback-err': 0,//nodejs 处理错误
    'id-length': 0,//变量名长度
    'init-declarations': 0,//声明时必须赋初值
    'key-spacing': [0, { 'beforeColon': false, 'afterColon': true }],//对象字面量中冒号的前后空格
    'lines-around-comment': 0,//行前/行后备注
    'max-len': [0, 80, 4],//字符串最大长度
    'max-params': [0, 3],//函数最多只能有3个参数
    'new-cap': 0,//函数名首行大写必须使用new方式调用，首行小写必须用不带new方式调用
    'new-parens': 2,//new时必须加小括号
    'newline-after-var': 0,//变量声明后是否需要空一行
    'object-curly-spacing': [0, 'never'],//大括号内是否允许不必要的空格
    'object-shorthand': 0,//强制对象字面量缩写语法
    // 'one-var': 1,//连续声明 "operator-assignment": [0, "always"],//赋值运算符 += -=什么的
    'operator-linebreak': [2, 'after'],//换行时运算符在行尾还是行首
    'padded-blocks': 0,//块语句内行首行尾是否要空行
    'prefer-spread': 0,//首选展开运算
    'prefer-reflect': 0,//首选Reflect的方法
    // 'quote-props': [2, 'always'],//对象字面量中的属性名是否强制双引号 "radix": 2,//parseInt必须指定第二个参数
    'id-match': 0,//命名检测
    'require-yield': 0,//生成器函数必须有yield
    'semi-spacing': [0, { 'before': false, 'after': true }],//分号前后空格
    'sort-vars': 0,//变量声明时排序
    'space-after-keywords': [0, 'always'],//关键字后面是否要空一格
    'space-before-function-paren': [0, 'always'],//函数定义时括号前面要不要有空格
    // 'space-return-throw-case': 2,//return throw case后面要不要加空格
    'spaced-comment': 0,//注释风格要不要有空格什么的 "strict": 2,//使用严格模式
    'use-isnan': 2,//禁止比较时使用NaN，只能用isNaN()
    'valid-jsdoc': 0,//jsdoc规则
    'valid-typeof': 2,//必须使用合法的typeof的值
    'vars-on-top': 0,//var必须放在作用域顶部
    'wrap-iife': [2, 'inside'],//立即执行函数表达式的小括号风格
    'wrap-regex': 0,//正则表达式字面量用小括号包起来
    'yoda': [2, 'never']//禁止尤达条件
  }
}
