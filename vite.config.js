import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import eslintPlugin from 'vite-plugin-eslint'
const { resolve } = require('path')

// https://vitejs.dev/config/
export default defineConfig({
  base: '/vitemap/',
  plugins: [
    vue(),
    // 增加下面的配置项,这样在运行时就能检查eslint规范
    eslintPlugin({
      include: ['src/**/*.js', 'src/**/*.vue', 'src/*.js', 'src/*.vue']
    })
  ],
  resolve: {
    alias: {
      '*': resolve('node_modules'),
      '@': resolve('src'),
      '#': resolve('public')
    }
  },
  server: {
    port: 5174,
    hmr: { overlay: false },
    proxy: {
      '/api': {
        target: 'http://jsonplaceholder.typicode.com',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, '')
      }
    }
  }
})
